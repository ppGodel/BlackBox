/* contains dummy routines for compiling without certain features */
#include "graphplan.h"
#include <stdio.h>
void draw_edge(char* graphname ,int source, int target, char* type, int level);
void draw_node(vertex_t v, int time, char*  type);
char * concat(char * c1, char* c2){
  char* concat_c;
  concat_c = (char *)malloc(strlen(c1) + strlen(c2) + 1);
  strcpy(concat_c, c1);
  strcat(concat_c, c2);
  return concat_c;
}


FILE * fil;
int havelist = 0;
char* graph_name;
void setup_viewer(char * problem_name)
{
  graph_name = problem_name;
  printf("{\n");
  printf("\"name\" : \"\%s\"\n", problem_name);
  printf("}");

}

void wait_until_left(void)
{ 
  //printf("\nwwwwwwwwwwwwwwwwwwwwww\n");
}

void reset_viewer(int max_time)
{ 
  //printf("\nvvvvvvvvvvvvvvvvvvvv\n");
}

void draw_edge(char* graphname ,int source, int target, char* type, int level){
    printf("\n");
    printf("{\n");
    printf("\"graph_name\" : \"%s\",\n", graphname);  
    printf("\"source\" : %d,\n", source);    
    printf("\"target\" : \"%d\",\n", target);
    printf("\"type\" : \"%s\",\n", type);
    printf("\"level\" : \"%d\"\n", level);
    printf("}");
}

void draw_node(vertex_t v, int time, char*  type)
{
  printf("\n");
  printf("{\n");
  printf("\"graph_name\" : \"%s\",\n", graph_name);  
  printf("\"node_name\" : \"%s\",\n",v->name);  
  printf("\"hash\" : \"%d\",\n", v->hashval);  
  printf("\"level\" : %d,\n", time ); 
  //  printf("\"is_true\" : %d,\n", v->is_true );  
  //  printf("\"uid_mask\" : %d,\n", v->uid_mask );  
  //  printf("\"uid_block\" : %d,\n", v->uid_block );  
  //  printf("\"cant_do\" : %d,\n", v->cant_do );  
  printf("\"type\" : \"%s\"\n", type );  
  printf("}\n");
  edgelist_t e;  
  int i = 0;
  for(e = v->out_edges; e; e = e->next)
  {
    if( i > 0 ){      
      printf(",");
    }
    draw_edge(graph_name, v->hashval, e->endpt->hashval, "o", time);
    i++;    
  }
  i = 0;
  for(e = v->in_edges; e; e = e->next)
  {
    if( i > 0 ){      
      printf(",");
    }
    draw_edge(graph_name, v->hashval, e->endpt->hashval, "i", time);
    i++;    
  }
  i = 0;
  for(e = v->del_edges; e; e = e->next)
  {
    if( i > 0 ){      
      printf(",");
    }
    draw_edge(graph_name, v->hashval, e->endpt->hashval, "d", time);
    i++;    
  }
  i = 0;
  for(e = v->exclusive; e; e = e->next)
  {
    if( i > 0 ){      
      printf(",");
    }
    draw_edge(graph_name, v->hashval, e->endpt->hashval, "m", time);
    i++;    
  }
  printf("\n");
  /* printf("\"eis_edges\" : [\n");  */
  /* i = 0; */
  /* for(e = v->excl_in_this_step; e; e = e->next) */
  /* { */
  /*   if( i > 0 ){       */
  /*     printf(",\n"); */
  /*   } */
  /*   printf("{\n"); */
  /*   printf("\"Num\" : %d,\n", i);     */
  /*   printf("\"vid\" : \"%d_%d\",\n",time+1, e->endpt->hashval); */
  /*   printf("\"hash\" : \"%d\",\n", e->endpt->hashval); */
  /*   printf("\"name\" : \"%s\"\n", e->endpt->name); */
  /*   printf("}"); */
  /*   i++; */
  /*   //u = e->endpt->next_time;  /\* the fact for OUR in-edge *\/ */
  /*   //w->in_edges = insert_edge_at_end(w->in_edges, u); */
  /*   //u->out_edges = insert_edge_at_end(u->out_edges, w); */
    
  /* } */
  //printf("%d", sizeof(v->in_edges));
}

void draw_fact(vertex_t v, int time, int flag)
{
  draw_node(v,time, "f");
}

void draw_op(vertex_t v, int time, int flag, int thick)
{
  draw_node(v,time, "a");
}

void do_final_viewing(void) {
  
  //printf("\n");      
  //printf("]\n");  
  //printf("}\n");
  //fclose(fil);
}

void do_graph_created() { 
  //printf("\nccccccccccccccccccc\n");
}

void handle_events() { 
  //printf("\neeeeeeeeeeeeeeeeeee\n");
}

